from numpy import sin, cos, pi, array
from scipy.integrate import odeint
import numpy as np
import matplotlib.pyplot as plt
import sys, getopt

class Pendulum:
	def __init__(self):
		self.g = 9.8
		self.m1 = 1
		self.r1 = 1
		self.m2 = 1
		self.r2 = 1
		self.t = np.linspace(0.0, 10.0, 10001)

	def set(self, parameters, time = 5):
		#reassigns parameters to input values
		self.g = parameters[0]			#gravity
		self.m1 = parameters[1]			#mass 1
		self.r1 = parameters[2]			#rod 1 length
		self.m2 = parameters[3]
		self.r2 = parameters[4]
		self.t =  np.linspace(0.0, time, 10001)

	def pendulum(self, initial, time):
		# theta1_ddot = -(m*g*r*sin(self.theta1)) / (self.i + (self.m1*self.r1*self.r1))
		theta1 = initial[0]
		theta1_dot = initial[1]
		theta1_ddot = -1*(self.g/self.r1) * np.sin(theta1)
		return [theta1_dot, theta1_ddot]

	def run(self, initial):
		state = odeint(self.pendulum, initial, self.t)
		return state

class Pendulum2(Pendulum):
	def pendulum(self, initial, time):
		theta1 = initial[0]
		theta1_dot = initial[1]
		theta2 = initial[2]
		theta2_dot = initial[3]

		theta1_ddot = ( -1*self.g*(2*self.m1+self.m2)*np.sin(theta1) - self.m2*self.g*np.sin(theta1-2*theta2) - 2*np.sin(theta1-theta2)*self.m2*(theta2_dot*theta2_dot*self.r2 + theta1_dot*theta1_dot*self.r1*np.cos(theta1-theta2))) / (self.r1*(2*self.m1 + self.m2 - self.m2*np.cos(2*theta1 - 2*theta2) ))

		theta2_ddot = ( 2*np.sin(theta1-theta2)* (theta1_dot*theta1_dot*self.r1* (self.m1+self.m2) + self.g*(self.m1+self.m2)*np.cos(theta1) + theta2_dot*theta2_dot*self.r2*self.m2*np.cos(theta1 - theta2)) ) / (self.r2*(2*self.m1 + self.m2 - self.m2*np.cos(2*theta1 - 2*theta2)) )

		return [theta1_dot, theta1_ddot, theta2_dot, theta2_ddot]
