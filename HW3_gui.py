# ---------------------------------------------------- #
# File: HW3_gui.py
# ---------------------------------------------------- #
# Author(s): Crystal Liu, Ivy McGinley
# BitBucket - user: BillieSue92
# ---------------------------------------------------- #
# Plaftorm:    Windows
# Environment: Python 2.7.7 :Anaconda 2.0.1
# Libaries:    numpy 1.9.0
#              matplotlib 1.4.0
#       	   scipy 0.14.0
#			   cv2 2.4.10
#			   Tkinter
#			   HW3_Pendulum
# ---------------------------------------------------- #
# Description: A GUI that animates and plots the 
# 	dynamics of a pendulum. The user can choose to view
#	either one or two pendulums.
# ---------------------------------------------------- #


#----------------------------------------------------------------#

#						IMPORTED MODULES                         #

#----------------------------------------------------------------#

import Tkinter as tk

import numpy as np
import matplotlib
matplotlib.use('TkAgg')

from numpy import sin, pi, cos

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import scipy.integrate as integrate

# Dyanmics
from HW3_Pendulum import Pendulum
from HW3_Pendulum import Pendulum2

#----------------------------------------------------------------#

#							GUI		                             #

#----------------------------------------------------------------#


class Application(tk.Frame):

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)

        # A flag to check if 2 pendulums are active
        self.pend_flag = False

        # Draws GUI
        self.CreateWidgets_1_Pendulum()
        self.pack()

    # Creates a slider
    def CreateSlider(self, slide_title = '', row = 0, column = 0, from_ = 0, to = 1, resolution = 1, 
    	rowspan = 0, columnspan = 0, set_value = 1):

		label = tk.Label(self, text = slide_title)
		label.grid(row = row, column = column)
		slide = tk.Scale(self, from_ = from_, to = to, resolution = resolution)
		slide.grid(row = row + 1, column = column, rowspan = rowspan)
		slide.set(set_value)

		return slide.get(), label, slide

	# Creates a button
    def CreateButton(self, label_flag = True, button_title = '', row = 0, column = 0, 
    	button_label = '', command = None, 
    	rowspan = 0, columnspan = 0, set_value = 1):

    	if label_flag:
			label = tk.Label(self, text = button_title)
			label.grid(row = row, column = column)

    	button = tk.Button(self, text = button_label, command = command)
    	button.grid(row = row + 1, column = column)

    	return button

   	# Creates an entry box
    def CreateEntryBox(self, entry_title = '', row = 0, column = 0, set_value = ''):

		# Creating entry box for gravity
		label = tk.Label(self, text = entry_title)
		label.grid(row = row, column = column)
		entry_var = tk.StringVar()
		entry = tk.Entry(self,textvariable = entry_var)
		entry.grid(row = row + 1, column = column)
		entry_var.set(set_value)

		return float(entry_var.get())

	# Creates widgets for one pendulum
    def CreateWidgets_1_Pendulum(self):

    	# Two buttons to choose one pendulum or two
		self.pend_1 = self.CreateButton(button_title = 'Pendulums', row = 1, column = 0, 
			button_label = 'one', command = self.removeWidgets)

		self.pend_2 = self.CreateButton(label_flag = False, row = 2, column = 0, 
			button_label = 'two', command = self.CreateWidgets_2_Pendulums)

    	# Sliders for one pendulum - angle 1, angular velocity 1, mass 1, rod length 1
		self.angle1 = self.CreateSlider(slide_title = u"\N{GREEK SMALL LETTER THETA} 1", row = 1, 
			column = 1, from_ = 1, to = 180, resolution = 1, rowspan = 2, set_value = 90 )

		self.velocity1 = self.CreateSlider(slide_title = u"\N{GREEK SMALL LETTER OMEGA} 1", row = 1,
			column = 2, from_ = -5, to = 5, resolution = 0.1, rowspan = 2, set_value = 0 )

		self.mass1 = self.CreateSlider(slide_title = "Mass 1", row = 1, column = 3, from_ = 0.5, to = 10,
			resolution = 0.5, rowspan = 2, set_value = 5 )

		self.length1 = self.CreateSlider(slide_title = "Length 1", row = 1, column = 4, from_ = 0.5, to = 10,
			resolution = 0.5, rowspan = 2, set_value = 5 )

		# Gravity entry box
		self.gg = self.CreateEntryBox(entry_title = 'Gravity [m/s/s]', row = 1, column = 9, set_value = '9.81')

		# Creating plot drop-down menu
		self.plot_label = tk.Label(self, text = 'Plot y-axis')
		self.plot_label.grid(row = 3, column = 9)
		self.plot_var = tk.StringVar()
		self.plot_var.set(u"\N{GREEK SMALL LETTER THETA} 1")

		self.plot_plot = tk.OptionMenu(self, self.plot_var, u"\N{GREEK SMALL LETTER THETA} 1",
			u"\N{GREEK SMALL LETTER OMEGA} 1")
		self.plot_plot.grid(row = 4, column = 9)

		# Plot and Quit buttons
		self.plot = self.CreateButton(label_flag = False, row = 1, column = 10, button_label = 'Plot',
			command = self.run_app)

		self.plot = self.CreateButton(label_flag = False, row = 1, column = 11, button_label = 'Quit', 
			command = self.quit)

	# Creates more widgets for two pendulums		
    def CreateWidgets_2_Pendulums(self):

    	# Disabling button for second pendulum
		self.pend_flag = True
		self.pend_2['state'] = 'disabled'

    	# Sliders for second pendulum - angle 2, angular velocity 2, mass 2, rod length 2		
		self.angle2 = self.CreateSlider(slide_title = u"\N{GREEK SMALL LETTER THETA} 2", row = 1, 
			column = 5, from_ = 1, to = 180, resolution = 1, rowspan = 2, set_value = 90 )

		self.velocity2 = self.CreateSlider(slide_title = u"\N{GREEK SMALL LETTER OMEGA} 2", row = 1,
			column = 6, from_ = -5, to = 5, resolution = 0.1, rowspan = 2, set_value = 0 )

		self.mass2 = self.CreateSlider(slide_title = "Mass 2", row = 1, column = 7, from_ = 0.5, to = 10,
			resolution = 0.5, rowspan = 2, set_value = 5 )

		self.length2 = self.CreateSlider(slide_title = "Length 2", row = 1, column = 8, from_ = 0.5, to = 10,
			resolution = 0.5, rowspan = 2, set_value = 5 )

		# Overwriting the plot option menu to have more choices for 2nd pendulum
		self.plot_plot = tk.OptionMenu(self, self.plot_var, u"\N{GREEK SMALL LETTER THETA} 1",
			u"\N{GREEK SMALL LETTER OMEGA} 1", u"\N{GREEK SMALL LETTER THETA} 2",
			u"\N{GREEK SMALL LETTER OMEGA} 2")
		self.plot_plot.grid(row = 4, column = 9)

	# Destroying 2nd pendulum widgets
    def removeWidgets(self):

    	if self.pend_flag:

	    	# Reseting flag
			self.pend_flag = False

			# Destroying widgets
			self.angle2[1].destroy(), self.angle2[2].destroy()
			self.velocity2[1].destroy(), self.velocity2[2].destroy()
			self.mass2[1].destroy(), self.mass2[2].destroy()
			self.length2[1].destroy(), self.length2[2].destroy()

			# Enabling two pendelum button
			self.pend_2['state'] = 'active'

	# Applying new conditions and running application
    def run_app(self):

    	# Gathering parameters and states from GUI user-inputs
    	self.tt = 20							# time for plotting
    	parameters = [0, 0, 0, 0, 0]			# g, mass 1, rod length 1, mass 2, rod length 2
    	initial= [0, 0, 0 ,0]					# theta 1, theta_dot 1, theta 2, theta_dot 2

    	parameters[0] = self.gg 				# Gravity from entry box
    	parameters[1] = self.mass1[0]			# Mass 1 from slider
    	parameters[2] = self.length1[0]			# Length 1 from slider

    	initial[0] = self.angle1[0]* np.pi/180	# Angle 1 from slider
    	initial[1] = self.velocity1[0]			# Angular velocity from slider

    	# Calculating dynamics for single pendulum
    	one_pendulum = Pendulum()
    	one_pendulum.set(parameters, self.tt)
    	self.states = one_pendulum.run(initial)

    	# Calculating dynamics for double pendulum
    	if self.pend_flag == True:

    		parameters[3] = self.mass2[0]			# Mass from slider
    		parameters[4] = self.length2[0]			# Length from slider

    		initial[2] = self.angle2[0] * np.pi/180	# Angle from slider
    		initial[3] = self.velocity2[0]			# Angular velocity from slider

    		two_pendulum = Pendulum2()
    		two_pendulum.set(parameters, 10)
    		self.states = two_pendulum.run(initial)

    	# Time increments for plotting
    	self.time = np.linspace(0, self.tt, len(self.states))

    	# Plot and animate dynamics
    	self.plot_dynamics()
    	self.animate_dynamics()

	# Plotting
    def plot_dynamics(self):

		# Creating plot window
		self.fig1 = plt.figure(figsize = (4,4), tight_layout = True)
		canvas = FigureCanvasTkAgg(self.fig1, master = self)
		canvas.get_tk_widget().grid(row = 0, column = 0, columnspan = 10)
		ax = self.fig1.add_subplot(111, xlabel = 'time [s]')
		ax.grid()

		# Grabbing data for plot		
		self.theta1_plot = self.states[:,0]
		self.theta_dot1_plot = self.states[:,1]
		self.theta2_plot = self.states[:,2]
		self.theta_dot2_plot = self.states[:,3]

		# Y-axis of the plot - choices: theta 1, theta_dot 1, theta 2, theta_dot 2
		plot_y = self.plot_var.get()

		#Plot theta 1 VS time
		if plot_y == u"\N{GREEK SMALL LETTER THETA} 1":		
			ax.plot(self.time,self.theta1_plot)
			ax.set_ylabel("Angle 1 [rad]")

		# Plot theta_dot 1 VS time		
		if plot_y == u"\N{GREEK SMALL LETTER OMEGA} 1":		
			ax.plot(self.time,self.theta_dot1_plot)
			ax.set_ylabel("Angular Velocity 1 [rad/s]")

		# Plot theta 2 VS time			
		if plot_y == u"\N{GREEK SMALL LETTER THETA} 2":		
			ax.plot(self.time,self.theta2_plot)
			ax.set_ylabel("Angle 2 [rad]")

		# Plot theta_dot 2 VS time			
		if plot_y == u"\N{GREEK SMALL LETTER OMEGA} 2":		
			ax.plot(self.time,self.theta_dot2_plot)
			ax.set_ylabel("Angular Velocity 2 [rad/s]")

	# Animating pendulum
    def animate_dynamics(self):

		# Creating animation window
		self.fig2 = plt.figure(figsize = (4,4))
		canvas = FigureCanvasTkAgg(self.fig2, master=self)
		canvas.get_tk_widget().grid(row = 0, column = 10, columnspan = 10)

		ax2 = plt.axes(xlim=(-25, 25), ylim=(-25, 25))
		ax2.grid()

		# Prepping the rods of pendulums for animation
		self.line1, = ax2.plot([], [], 'o-' , lw=2)
		self.line2, = ax2.plot([], [], 'o-' , lw=2)
		self.line1.set_data([], [])
		self.line2.set_data([], [])

		# Animation
		self.anim = animation.FuncAnimation(self.fig2, self.animate, 40*np.arange(1, len(self.states)),
			init_func=self.init, interval=20, blit=False, repeat = False)

		canvas.show()

	# For animation - initialization function
    def init(self):
		return self.line1, self.line2,

    # Animation function
    def animate(self, i):

    	# Gathering position (x,y) of pendulum for animation
    	# One pendulum animation
    	xx1 = self.length1[0] * np.sin(self.theta1_plot)
    	yy1 = -self.length1[0] * np.cos(self.theta1_plot)
    	thisx = [0, xx1[i]]
    	thisy = [0, yy1[i]]
    	self.line1.set_data(thisx, thisy)

    	# Two pendulum animation
    	if self.pend_flag == True:
    		xx2 = xx1 + self.length2[0] * np.sin(self.theta2_plot)
    		yy2 = yy1 - self.length2[0] * np.cos(self.theta2_plot)
    		thisx2 = [xx1[i], xx2[i]]
    		thisy2 = [yy1[i], yy2[i]]
    		self.line2.set_data(thisx2, thisy2)

    	return self.line1, self.line2

root = tk.Tk()

app = Application(master=root)
app.master.title("Pendulum simulation")
app.mainloop()

root.destroy()
