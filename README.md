# ---------------------------------------------------- #
# File: HW3_Pseudo-code.txt
# ---------------------------------------------------- #
# Author(s): Crystal Liu, Ivy McGinley
# BitBucket - user: BillieSue92
# ---------------------------------------------------- #
# Description: A GUI that animates and plots the 
# 	dynamics of a pendulum. The user can choose to view
#	either one or two pendulums.
# ---------------------------------------------------- #

\\\\\Multi-Body Dynamics\\\\\

Pendulum class
	
	#Global parameters (can vary)
		gravity = g
		mass 1 = m1
		rod length 1 = r1
		mass2 = m2
		rod length 2 = r2
		time array = t

	#Single pendulum dynamics function
	# Function inputs – theta, theta_dot

		#Pendulum’s equation of motion (no input torque)
		Theta_ddot = -(m * g * r * sin(theta)) / (I + (m * r **2))

	#Double pendulum dynamics function
	#Function inputs – theta1, theta_dot1, theta2, theta_dot2
		#Varying initial values
		mass 2 = m2
		rod length 2 = r2
		moment of inertia 2 = i2

		#Pendulums' equation of motion (no input torque)
		theta_ddot1 = (- g * (2 * m1 + m2) * sin(theta1) - m2 * g * sin(theta1 - 2 * theta2) - 2 * sin(theta1 - theta2) * m2 * (theta_dot2 ** 2 * r2 + theta_dot1 ** 2 * r1 * cos(theta1 - theta2))) / (r1 * (2 * m1 + m2 - m2 * cos(2 * theta1 - 2 * theta2)))

		theta_ddot2 = (2 * sin(theta1 - theta2) * theta_dot1 ** 2 * r1 * (m1 + m2) + g * (m1 + m2) * cos(theta1) + 	theta_dot2 ** 2 * r2 * m2 * cos(theta1 - theta2)) / (r2 * (2 * m1 + m2 - m2 * cos(2 * theta1 - 2 * theta2)))

	#Global functions
		Set Function
			sets the values of the parameters

		Run Function
			integrates the dynamics functions through odeint
			returns theta and theta_dot for each t in the time array

\\\\\GUI\\\\\

\\Draw GUI
	def __init__

		Create frame
		Create pendulum flag #determines whether there is one or two pendulums
		Draw gui

	def CreateSlider

		create the label
		create the slider
		set the intial value

		return the slider value, label object, and slide object

	def CreateButton

    	if label desired:
			create label

		create button

    	return button object

	def CreateEntryBox

		create label
		create entry box
		set entry value

		return entry value as float

	def CreateWidgets_1_Pendulum

		Create pendulum buttons for number of pendulums

		Create sliders
			angle 1 
			angular velocity 1
			mass 1
			rod length 1

		Create entry box for gravity

		Create plot drop-down menu
			angle1
			angular velocity1

		Create buttons - Plot, Quit
			Plot
			Quit

	def CreateWidgets_2_Pendulums

		disable button for 2nd pendulum choice

		create sliders for second pendulum

		Draw 4 more sliders for 2nd pendulum
				angle 2
				angular velocity 2
				mass 2
				rod length 2

		Draw two more to drop-down list for y-axis of plot
			angle 2
			angular velocity 2

		overwrite the plot option menu to have more choices for the second pendulum

	def removeWidgets

		removes the sliders for second pendulum if one pendulum is selected

	def run_app

		apply new conditions and running applicaiton
		gather parameters and states from gui user inputs
		calculate dynamics for single pendulum
		calculate dynamincs for double pendulum if double pendulum is selected
		plot dynamics
		animate dynamics

	def plot_dynamics

		create a plotting window
		grab data for the plot
		y-axis of the plot - choices: theta1, theta_dot 1, theta2, theta_dot2
		plot theta1 vs time
		plot theta_dot 1 vs time
		plot theta 2 vs time
		plot theta_dot 2 vs time

	def animate_dynamics

		create animation window
		animate

	def init
		initialize animation function

	def animate
		animation function

Initialize application

\\\\\END\\\\\
